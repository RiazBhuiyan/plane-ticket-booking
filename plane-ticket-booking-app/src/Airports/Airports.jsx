import React, { useState, useEffect } from 'react';
import { getAirports } from '../apiService';

const Airports = () => {
  const [airports, setAirports] = useState([]);

  useEffect(() => {
    const fetchAirports = async () => {
      try {
        const fetchedAirports = await getAirports();
        setAirports(fetchedAirports);
      } catch (error) {
        console.error('Error fetching airports:', error.message);
      }
    };

    fetchAirports();
  }, []);

  return (
    <div className='flex flex-col items-center justify-center h-full'>
      <h1 className='text-4xl font-semi-bold mb-6'>List of Airports</h1>
      <div className='grid grid-cols-3 gap-4'>
        {airports.map((airport) => (
          <div
            key={airport.id}
            className='bg-[#202020] p-4 rounded-lg shadow-md text-white'
          >
            <p className='font-semibold'>Airport ID: {airport.id}</p>
            <p className='font-semibold'>Airport Code: {airport.code}</p>
            <p className='font-semibold'>Airport Title: {airport.title}</p>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Airports;
