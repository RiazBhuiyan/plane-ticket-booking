import React from 'react';
import Header from './Header/Header';
import Airports from './Airports/Airports';
import BookFlight from './BookFlight/BookFlight';
import { Route, Routes } from 'react-router';
import HomePage from './HomePage/HomePage';
import { BookingProvider } from './BookingContext/BookingContext';

const App = () => {
  return (
    <BookingProvider>
      <Header />
      <Routes>
        <Route path='/' element={<HomePage />} />
        <Route path='/airports' element={<Airports />} />
        <Route path='/book-flight' element={<BookFlight />} />
      </Routes>
    </BookingProvider>
  );
};

export default App;
