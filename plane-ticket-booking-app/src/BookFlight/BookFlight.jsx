import React, { useState, useEffect } from 'react';
import ReactDatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import { createBooking } from '../apiService';
import { getAirports } from '../apiService';

const BookFlight = () => {
  const [formData, setFormData] = useState({
    firstName: '',
    lastName: '',
    departureAirportId: '',
    arrivalAirportId: '',
    departureDate: null,
    returnDate: null,
  });
  const [airports, setAirports] = useState([]);
  const [errorMessage, setErrorMessage] = useState('');

  useEffect(() => {
    const fetchAirports = async () => {
      try {
        const fetchedAirports = await getAirports();
        setAirports(fetchedAirports);
        console.log(fetchedAirports);
      } catch (error) {
        console.error('Error fetching airports:', error.message);
      }
    };

    fetchAirports();
  }, []);

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleDateChange = (date, name) => {
    setFormData({ ...formData, [name]: date });
  };

  const handleSubmit = () => {
    if (formData.departureAirportId === formData.arrivalAirportId) {
      setErrorMessage('Departure and arrival airports cannot be the same.');
      return;
    }
    setErrorMessage('');

    // Adjust the return date to be the right one
    const adjustedReturnDate = formData.returnDate
      ? new Date(formData.returnDate.getTime() + 24 * 60 * 60 * 1000)
      : null;

    const adjustedDepartureDate = formData.departureDate
      ? new Date(formData.departureDate.getTime() + 24 * 60 * 60 * 1000)
      : null;

    createBooking({
      ...formData,
      departureDate: adjustedDepartureDate,
      returnDate: adjustedReturnDate,
    });
    setFormData({
      firstName: '',
      lastName: '',
      departureAirportId: '',
      arrivalAirportId: '',
      departureDate: null,
      returnDate: null,
    });
  };

  return (
    <div className='relative max-w-7xl mx-auto sm:px-6 lg:px-8 mt-6'>
      <div className='flex flex-col mt-36 w-full'>
        <div className='max-h-1/2 p-4 bg-[#202020] rounded-xl m-4'>
          <h2 className='text-4xl text-center font-semibold mb-4 text-white'>
            Flight Reservation
          </h2>
          <input
            type='text'
            name='firstName'
            value={formData.firstName}
            onChange={handleInputChange}
            placeholder='First Name'
            className='w-full text-black border rounded-md p-2 mb-2'
          />
          <input
            type='text'
            name='lastName'
            value={formData.lastName}
            onChange={handleInputChange}
            placeholder='Last Name'
            className='w-full text-black border rounded-md p-2 mb-2'
          />
          <select
            name='departureAirportId'
            value={formData.departureAirportId}
            onChange={handleInputChange}
            className='w-full text-black border rounded-md p-2 mb-2'
          >
            <option value=''>Select Departure Airport</option>
            {airports.map((airport) => (
              <option key={airport.id} value={airport.id}>
                {airport.title} ({airport.code})
              </option>
            ))}
          </select>
          <select
            name='arrivalAirportId'
            value={formData.arrivalAirportId}
            onChange={handleInputChange}
            className='w-full text-black border rounded-md p-2 mb-4'
          >
            <option value=''>Select Arrival Airport</option>
            {airports.map((airport) => (
              <option key={airport.id} value={airport.id}>
                {airport.title} ({airport.code})
              </option>
            ))}
          </select>
          {/* Date pickers */}
          <div className='grid grid-cols-2'>
            <div className='flex flex-col items-center justify-center rounded-tl-lg rounded-bl-lg border border-gray-700 text-white px-4 py-2'>
              <span className='font-semibold flex justify-center items-center'>
                Check-in
              </span>
              <ReactDatePicker
                placeholderText='Pick start date'
                selected={formData.departureDate}
                onChange={(date) => handleDateChange(date, 'departureDate')}
                dateFormat='dd.MM.yyyy'
                minDate={new Date()}
                className='w-full text-center bg-[#202020] dark:text-white focus:outline-none focus:border-gray-900'
              />
            </div>
            <div className='flex flex-col items-center justify-center rounded-tr-lg rounded-br-lg border border-l-0 border-gray-700 text-white px-4 py-2'>
              <span className='font-semibold flex justify-center items-center'>
                Check-out
              </span>
              <ReactDatePicker
                placeholderText='Pick end date'
                selected={formData.returnDate}
                onChange={(date) => handleDateChange(date, 'returnDate')}
                dateFormat='dd.MM.yyyy'
                minDate={
                  formData.departureDate
                    ? new Date(
                        formData.departureDate.getTime() + 24 * 60 * 60 * 1000
                      )
                    : new Date()
                }
                className='w-full text-center bg-[#202020] dark:text-white focus:outline-none focus:border-gray-900'
              />
            </div>
          </div>
          {errorMessage && <p style={{ color: '#FF4F4B' }}>{errorMessage}</p>}
          <div className='mt-4'>
            <button
              onClick={handleSubmit}
              className='w-full text-black font-bold bg-[#c2df2b] py-3 px-4 rounded-lg'
            >
              Book
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default BookFlight;
