import React from 'react';
import { deleteBooking } from '../apiService';

const BookingContainer = ({ booking, onDelete }) => {
  const handleDelete = async () => {
    try {
      const deleted = await deleteBooking(booking.id);
      if (deleted) {
        onDelete(booking.id);
        console.log('Booking deleted successfully');
      } else {
        console.log('Failed to delete booking');
      }
    } catch (error) {
      console.error('Error deleting booking:', error.message);
    }
  };

  const departureDate = new Date(booking.departureDate).toLocaleDateString();
  const returnDate = new Date(booking.returnDate).toLocaleDateString();

  return (
    <main>
      <div className='relative max-w-7xl mx-auto sm:px-6 lg:px-8 mt-6 mb-6'>
        <div className='relative shadow-lg sm:border-4 border-none bg-[#252525] sm:rounded-2xl sm:overflow-hidden'>
          <div className='px-20 py-8 flex flex-col sm:flex-row'>
            <div className='w-full sm:w-1/2 flex flex-col justify-center items-center'>
              <h1 className='text-2xl font-bold tracking-tight sm:text-2xl lg:text-3xl '>
                <span className='block text-white'>Booking Information</span>
              </h1>
              <h1 className='text-2xl font-bold tracking-tight sm:text-2xl lg:text-3xl mt-4 '>
                <span className='block text-white'>ID: {booking.id}</span>
              </h1>
              <button
                onClick={handleDelete}
                className='bg-[#c2df2b] text-black font-bold py-2 px-20 rounded mt-4'
              >
                Delete Booking
              </button>
            </div>
            <div className='w-full sm:w-1/2 mt-6 sm:mt-0 text-white flex flex-col justify-center items-center'>
              <div className='grid grid-cols-2 gap-y-2 gap-x-20'>
                <p>First Name:</p>
                <p className='text-left'>{booking.firstName}</p>
                <p>Last Name:</p>
                <p className='text-left'>{booking.lastName}</p>
                <p>Departure Airport:</p>
                <p className='text-left'>{booking.departureAirportId}</p>
                <p>Arrival Airport:</p>
                <p className='text-left'>{booking.arrivalAirportId}</p>
                <p>Departure Date:</p>
                <p className='text-left'>{departureDate}</p>
                <p>Return Date:</p>
                <p className='text-left'>{returnDate}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>
  );
};

export default BookingContainer;
