import React, { useState, useContext } from 'react';

const BookingContext = React.createContext();

export const BookingProvider = ({ children }) => {
  const [searchedBooking, setSearchedBooking] = useState(null);
  const [isSearched, setIsSearched] = useState(false);

  const updateSearchedBooking = (booking) => {
    setSearchedBooking(booking);
    setIsSearched(true);
  };

  const clearSearchedBooking = () => {
    setSearchedBooking(null);
    setIsSearched(false);
  };

  return (
    <BookingContext.Provider
      value={{
        searchedBooking,
        isSearched,
        updateSearchedBooking,
        clearSearchedBooking,
      }}
    >
      {children}
    </BookingContext.Provider>
  );
};

export const useBookingContext = () => {
  return useContext(BookingContext);
};
