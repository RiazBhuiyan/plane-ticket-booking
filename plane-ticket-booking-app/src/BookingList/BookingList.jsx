import React, { useState, useEffect } from 'react';
import BookingContainer from '../BookingContainer/BookingContainer';
import { getBookings } from '../apiService';
import Pagination from '../Pagination/Pagination';
import { deleteBooking } from '../apiService';
import { useBookingContext } from '../BookingContext/BookingContext';

const BookingList = () => {
  const [bookings, setBookings] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalBookings, setTotalBookings] = useState(0);
  const itemsPerPage = 5;

  const { isSearched, searchedBooking } = useBookingContext();

  useEffect(() => {
    const fetchBookings = async () => {
      try {
        const fetchedBookings = await getBookings(currentPage - 1);
        setBookings(fetchedBookings.list);
        setTotalBookings(fetchedBookings.totalCount);
      } catch (error) {
        console.error('Error fetching bookings:', error.message);
      }
    };

    fetchBookings();
  }, [currentPage]);

  const handlePageChange = (pageNumber) => {
    setCurrentPage(pageNumber);
  };

  const handleDelete = async (deletedId) => {
    try {
      const deleted = await deleteBooking(deletedId);
      if (deleted) {
        const updatedBookings = await getBookings(currentPage - 1);
        const updatedBookingsList = updatedBookings.list;
        if (updatedBookingsList.length === 0 && totalBookings > 0) {
          const prevPage = Math.max(currentPage - 1, 1);
          const prevPageBookings = await getBookings(prevPage - 1);
          setBookings(prevPageBookings.list);
          setTotalBookings(prevPageBookings.totalCount);
          setCurrentPage(prevPage);
        } else {
          setBookings(updatedBookingsList);
          setTotalBookings(updatedBookings.totalCount);
        }
      }
    } catch (error) {
      console.error('Error deleting booking:', error.message);
    }
  };

  return (
    <div>
      {isSearched && searchedBooking.id ? (
        <BookingContainer
          key={searchedBooking.id}
          booking={searchedBooking}
          onDelete={handleDelete}
        />
      ) : isSearched ? (
        <p className='text-center mt-10 text-4xl'>No booking with such ID!</p>
      ) : null}
      {!isSearched && (
        <>
          {bookings.map((booking) => (
            <BookingContainer
              key={booking.id}
              booking={booking}
              onDelete={handleDelete}
            />
          ))}
          <Pagination
            currentPage={currentPage}
            totalPages={Math.ceil(totalBookings / itemsPerPage)}
            onPageChange={handlePageChange}
          />
        </>
      )}
    </div>
  );
};

export default BookingList;
