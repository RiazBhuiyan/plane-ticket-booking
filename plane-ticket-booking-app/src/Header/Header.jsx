import React from 'react';
import Logo from '../Logo/Logo';
import { motion } from 'framer-motion';
import { Link } from 'react-router-dom';

function Header() {
  return (
    <div className='flex justify-between items-center max-w-7xl mx-auto px-4 py-6 sm:px-6 lg:justify-start md:space-x-10 lg:px-8 '>
      <div className='flex justify-start  lg:w-0 lg:flex-1   '>
        <Link to='/' className='flex flex-row items-center'>
          <Logo />
          <div className='  text-2xl  ml-2  font-extrabold  sm:text-4xl'>
            <span className='font-bold text-[#c2df2b]'>AviaBook</span>
          </div>
        </Link>
      </div>
      <div className='flex space-x-5'>
        <motion.div
          className='-mt-2'
          whileHover={{
            translateY: 5,
            scale: 1.05,
            transition: { duration: 0.2 },
          }}
        >
          <Link
            to='/book-flight'
            className='border-b-4 border-[#c2df2b] py-2 px-4 text-base font-medium text-[#c2df2b]'
          >
            Book flight
          </Link>
        </motion.div>
        <motion.div
          className='-mt-2'
          whileHover={{
            translateY: 5,

            scale: 1.05,
            transition: { duration: 0.2 },
          }}
        >
          <Link
            to='/airports'
            className='border-b-4 border-[#c2df2b] py-2 px-4 text-base font-medium text-[#c2df2b]'
          >
            Airports
          </Link>
        </motion.div>
      </div>
    </div>
  );
}

export default Header;
