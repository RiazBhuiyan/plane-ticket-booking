import React from 'react';
import Hero from '../Hero/Hero';
import SectionTitle from '../SectionTitle/SectionTitle';
import BookingList from '../BookingList/BookingList';

const HomePage = () => {
  return (
    <div>
      <Hero />
      <SectionTitle />
      <BookingList />
    </div>
  );
};

export default HomePage;
