import React from 'react';

const Pagination = ({ currentPage, totalPages, onPageChange }) => {
  const pageNumbers = [];

  for (let i = 1; i <= totalPages; i++) {
    pageNumbers.push(i);
  }

  const handlePageChange = (number) => {
    onPageChange(number);
    const scrollToListStart = Math.ceil(window.innerHeight / 1.35);
    window.scrollTo(0, scrollToListStart);
  };

  return (
    <div className='flex justify-center items-center mt-10'>
      {pageNumbers.map((number) => (
        <button
          key={number}
          onClick={() => handlePageChange(number)}
          className={`${
            currentPage === number
              ? 'bg-[#c2df2b] hover:bg-[#adc050] text-black'
              : 'bg-gray-200 text-gray-600 hover:bg-gray-300'
          } px-4 py-2 rounded-md font-bold mx-1 mb-10`}
        >
          {number}
        </button>
      ))}
    </div>
  );
};

export default Pagination;
