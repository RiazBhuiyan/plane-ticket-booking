import React, { useState } from 'react';
import { SearchCircleIcon } from '@heroicons/react/outline';
import { getSingleBooking } from '../apiService';
import { useBookingContext } from '../BookingContext/BookingContext';

export default function SearchBookings() {
  const [value, setValue] = useState('');
  const { updateSearchedBooking, clearSearchedBooking, isSearched } =
    useBookingContext();

  const handleSearch = async (bookingId) => {
    try {
      if (bookingId.trim() !== '') {
        const booking = await getSingleBooking(bookingId);
        updateSearchedBooking(booking);
      } else {
        clearSearchedBooking();
      }
    } catch (error) {
      console.error('Error fetching booking details:', error.message);
    }
  };

  const handleInputChange = (e) => {
    const inputValue = e.target.value;
    setValue(inputValue);
    handleSearch(inputValue);
  };

  const inputProps = {
    placeholder: 'Search by Booking ID',
    value,
    onChange: handleInputChange,
    className:
      'text-xl h-16 ml-4 px-4 py-2 rounded-lg bg-[#202020] text-white focus:outline-none',
  };

  return (
    <div>
      <div className='flex items-center w-full h-28 bg-[#202020] justify-between p-4 rounded-full border-4 border-[#c2df2b]'>
        <input type='text' {...inputProps} />
        <button
          type='button'
          className='inline-flex justify-center items-center text-base font-medium text-black'
        >
          <SearchCircleIcon
            className='h-20 text-[#c2df2b]'
            aria-hidden='true'
          />
        </button>
      </div>
    </div>
  );
}
