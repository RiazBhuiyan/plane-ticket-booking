import React from 'react';

const SectionTitle = () => {
  return (
    <div className='relative max-w-7xl mx-auto sm:px-6 lg:px-8 mt-6'>
      <div className='flex flex-col mt-36 w-full'>
        <h1 className='text-3xl font-bold tracking-tight text-white'>
          Booked flights
        </h1>
        <div className='border-b border-white w-full mt-2'></div>
      </div>
    </div>
  );
};

export default SectionTitle;
