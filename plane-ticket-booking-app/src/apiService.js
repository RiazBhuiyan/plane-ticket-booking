const API_BASE_URL = 'https://interview.fio.de/core-frontend/api';
const API_TOKEN = 'lZb7dG4l1Jb8DgyFfNb2v1AH6J6cJK';

// Function to handle API calls to get airports
const getAirports = async () => {
  try {
    const response = await fetch(`${API_BASE_URL}/airports`, {
      headers: {
        Authorization: `Bearer ${API_TOKEN}`,
        'Content-Type': 'application/json',
      },
    });

    if (!response.ok) {
      throw new Error('Network response was not ok');
    }

    return await response.json();
  } catch (error) {
    console.error('Error fetching airports:', error.message);
    return [];
  }
};

// Function to handle API calls to get bookings
const getBookings = async (pageIndex = 0, pageSize = 5) => {
  try {
    const response = await fetch(
      `${API_BASE_URL}/bookings?pageIndex=${pageIndex}&pageSize=${pageSize}&authToken=${API_TOKEN}`,
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
        },
      }
    );

    if (!response.ok) {
      throw new Error('Network response was not ok');
    }

    const data = await response.json();
    return data; // Return the list of bookings
  } catch (error) {
    console.error('Error fetching bookings:', error.message);
    throw error;
  }
};

// Function to handle API call to get details of a single booking
const getSingleBooking = async (bookingId) => {
  try {
    const response = await fetch(
      `${API_BASE_URL}/bookings/${bookingId}?authToken=${API_TOKEN}`,
      {
        headers: {
          Authorization: `Bearer ${API_TOKEN}`,
          'Content-Type': 'application/json',
        },
      }
    );

    if (!response.ok) {
      throw new Error('Network response was not ok');
    }

    return await response.json();
  } catch (error) {
    console.error('Error fetching single booking:', error.message);
    return null;
  }
};

// Function to handle creating a booking via API
const createBooking = async (bookingData) => {
  try {
    const response = await fetch(
      `${API_BASE_URL}/bookings/create?authToken=${API_TOKEN}`,
      {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${API_TOKEN}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(bookingData),
      }
    );

    if (!response.ok) {
      throw new Error('Failed to create booking');
    }

    return await response.json();
  } catch (error) {
    console.error('Error creating booking:', error.message);
    return null;
  }
};

// Function to handle deleting a booking via API
const deleteBooking = async (bookingId) => {
  try {
    const response = await fetch(
      `${API_BASE_URL}/bookings/delete/${bookingId}?authToken=${API_TOKEN}`,
      {
        method: 'DELETE',
        headers: {
          Authorization: `Bearer ${API_TOKEN}`,
          'Content-Type': 'application/json',
        },
      }
    );

    if (!response.ok) {
      throw new Error('Failed to delete booking');
    }

    return true;
  } catch (error) {
    console.error('Error deleting booking:', error.message);
    return false;
  }
};

export {
  getAirports,
  getBookings,
  getSingleBooking,
  createBooking,
  deleteBooking,
};
